var map;
var marker;
var date;
var str;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 43.47, lng: -3.82},
    zoom: 8
  });
  marker = new google.maps.Marker({
        position: {lat: 43.47, lng: -3.82}, 
        title:"Click to view info!"
        });
}
$(document).ready(
    function(){
      //* Latitud Santander: 43.4722475
      //* Longitud Santander: -3.8199358 
      var lat=43.085;
      var lng=-1.62383;
      
      google.maps.event.addListener(map, "click", function (e) {
        //lat and lng is available in e object
        var latLng = e.latLng;
        lat=e.latLng.lat();
        lng=e.latLng.lng();
        
        marker.setMap(null); 
        marker = new google.maps.Marker({
        position: latLng, 
        title:"Click to view info!"
        });
        map.setCenter(marker.position);
        marker.setMap(map); 

        var api_key_weather = "05368a30f30aa3de0d153301a9ac52f6";
        var api = "https://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lng+"&units=metric&appid="+api_key_weather;
        var api2 = "https://api.openweathermap.org/data/2.5/forecast?lat="+lat+"&lon="+lng+"&units=metric&appid="+api_key_weather;

        var jqxhr = $.getJSON( api)
              .done(function(data) {
                    $( "#temperatura" ).html(data.main.temp+" ℃");
              })
              .fail(function(data) {
                    $( "#temperatura" ).html( "Lo siento! no es posible conectar con nuestros servicio de climas" );
                    console.log( "error conectando a API openweathermap.org" );
              })
              .always(function() {
                    //cuando termina, no lo utilizamos
              });

        var jqxhr2 =$.getJSON( api2)
              .done(function(data) {
                  
                  $( "#pronostico" ).html("");
                  for (var i =0 ; i <= 6; i++) {
                    str = data.list[i].dt_txt;
                    date = str.substring(10, 16);
                     
                   $( "#pronostico" ).append(" **"+date+ " Temp: "+data.list[i].main.temp+" ℃ "+"<img src=\"http://openweathermap.org/img/w/"+data.list[i].weather[0].icon+".png\" height=\"42\" width=\"42\"></img>");
                  }
              })
              .fail(function(data) {
                    $( "#pronostico" ).html( "Lo siento! no es posible conectar con nuestros servicio de climas" );
                    console.log( "error conectando a API openweathermap.org" );
              })
              .always(function() {
                    //cuando termina, no lo utilizamos
              });


        

      });
    }
);

//apiMap AIzaSyAK83sAH9mtERVSDBJsxijgi89tPRLoiAQ